﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Diagnostics;
using System.IO;

namespace MongoExportImport
{
    public class Program
    {
        public static string Database => "_shahadath_Treaty";
        public static string MongoToolsRelativePath => @"D:\Mongo_Import\mongo-tools\mongoimport";
        static void Main(string[] args)
        {
            string relativePath = @"C:\Users\shahadath.ORBITAXBD\Downloads\_shahadath_Treaty_1\_shahadath_Treaty";
            import(relativePath);
        }


        static void import(string relativePath)
        {
            var path = Directory.GetFiles(relativePath);
            foreach (var filePath in Directory.GetFiles(relativePath))
            {
                using (Process process = new Process())
                {
                    var fileName = Path.GetFileName(filePath);
                    process.StartInfo.FileName = "CMD.exe";
                    process.StartInfo.WorkingDirectory = MongoToolsRelativePath;
                    process.StartInfo.Arguments = string.Format(@$"/C mongoimport -d {Database} -c {Path.GetFileNameWithoutExtension(fileName)} --file {filePath}");
                    process.Start();
                    //process.StartInfo.ArgumentList.Add(string.Format("git push {0} {1}", remoteName, branch));
                    process.WaitForExit();
                }
            }

        }
    }
}
